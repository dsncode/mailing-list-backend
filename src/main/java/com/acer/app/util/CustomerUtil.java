package com.acer.app.util;

import com.acer.app.beans.Customer;
import com.acer.app.beans.value.CustomerId;
import com.acer.app.beans.value.Email;
import com.acer.app.beans.value.Password;
import com.acer.app.exception.InvalidEmailException;
import com.acer.app.exception.InvalidPasswordException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class CustomerUtil {

	private static ObjectMapper om = new ObjectMapper();
	public static ObjectNode toJsonNode(Customer customer)
	{
		return om.createObjectNode()
				.put("name", customer.getName())
				.put("password", customer.getPassword().getValue())
				.put("email", customer.getEmail().getValue())
				.put("subscribed", customer.isSubscrived());
	}
	
	public static  Customer fromJsonNode(CustomerId id, JsonNode node) throws InvalidEmailException, InvalidPasswordException
	{
		return new Customer(id,
				node.path("name").asText(),
				Email.build(node.path("email").asText()), 
				Password.build(node.path("password").asText()),
				node.path("subscribed").asBoolean());
	}
	
	
}
