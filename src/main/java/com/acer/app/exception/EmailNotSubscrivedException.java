package com.acer.app.exception;

public class EmailNotSubscrivedException extends Exception{

	private static final long serialVersionUID = 5253098517293803485L;

	public EmailNotSubscrivedException(String msg)
	{
		super(msg);
	}
	
}
