package com.acer.app.exception;

public class CustomerException extends Exception{

	private static final long serialVersionUID = -684762280394363615L;

	public CustomerException(String msg)
	{
		super(msg);
	}
	
}
