package com.acer.app.exception;

public class CustomerNotFoundException extends CustomerException{

	private static final long serialVersionUID = -8678894005344416581L;

	public CustomerNotFoundException(String msg)
	{
		super(msg);
	}
	
}
