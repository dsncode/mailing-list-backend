package com.acer.app.exception;

public class DBException extends Exception{

	private static final long serialVersionUID = 3339560295957923075L;

	public DBException(String msg)
	{
		super(msg);
	}
	
}
