package com.acer.app.exception;

public class InvalidEmailException extends CustomerException{

	private static final long serialVersionUID = 945018704295863768L;

	public InvalidEmailException(String msg)
	{
		super(msg);
	}
	
}
