package com.acer.app.exception;

public class InvalidPasswordException extends CustomerException{

	private static final long serialVersionUID = -913847471539757966L;

	public InvalidPasswordException(String msg) {
		super(msg);
	}

}
