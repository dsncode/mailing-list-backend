package com.acer.app.beans.value;

import java.util.regex.Pattern;

import com.acer.app.exception.InvalidEmailException;

public final class Email {
	
	private final String email;
	private static Pattern regex = Pattern.compile("\\b[\\w.%-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b");
	
	public static Email build(String email) throws InvalidEmailException
	{
		if(!regex.matcher(email).matches())
			throw new InvalidEmailException("invalid email "+email);
		return new Email(email);
	}
	
	private Email(String email)
	{
		this.email=email;
	}
	
	@Override
	public boolean equals(Object value)
	{
		if(email.contentEquals(value.toString()))
			return true;
		else
			return false;
	}

	@Override
	public int hashCode()
	{
		return this.email.hashCode();
	}
	
	@Override
	public String toString()
	{
		return this.email;
	}
	
	public String getValue()
	{
		return email;
	}
	
}
