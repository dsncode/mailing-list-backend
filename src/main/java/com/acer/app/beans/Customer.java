package com.acer.app.beans;

import com.acer.app.beans.value.CustomerId;
import com.acer.app.beans.value.Email;
import com.acer.app.beans.value.Password;

public class Customer {

	private final CustomerId userid;
	private final Email email;
	private final String name;
	private final Password password;
	private final boolean subscribed;
	public Customer(CustomerId userid, String name, Email email, Password password,boolean subscrived)
	{
		this.name=name;
		this.userid=userid;
		this.email=email;
		this.subscribed=subscrived;
		this.password=password;
	}
	
	public CustomerId getCustomerId() {
		return userid;
	}
	
	public Email getEmail()
	{
		return email;
	}
	
	public String getName() {
		return name;
	}
	
	public Password getPassword()
	{
		return password;
	}
	
	public boolean isSubscrived()
	{
		return subscribed;
	}
	
}
