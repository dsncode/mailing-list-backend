package com.acer.app.service.impl;

import com.acer.app.beans.Customer;
import com.acer.app.beans.value.CustomerId;
import com.acer.app.beans.value.Email;
import com.acer.app.beans.value.Password;
import com.acer.app.dao.CustomerDao;
import com.acer.app.dao.impl.CustomerDaoImpl;
import com.acer.app.exception.CustomerException;
import com.acer.app.exception.DBException;
import com.acer.app.exception.EmailNotSubscrivedException;
import com.acer.app.service.CustomerService;
import com.acer.app.service.EmailSubscriptionService;

public class CustomerServiceImpl implements CustomerService{

	private CustomerDao customerDao;  
	private EmailSubscriptionService emailService; 
		
	private CustomerServiceImpl(CustomerDao customerDao, EmailSubscriptionService emailService)
	{
		this.customerDao = customerDao;
		this.emailService = emailService;
	}
	
	public static CustomerServiceImpl build(CustomerDao customerDao,EmailSubscriptionService emailService)
	{
		return new CustomerServiceImpl(customerDao,emailService);
	}
	
	public Customer findCustomer(CustomerId id) throws CustomerException
	{
		return customerDao.findCustomer(id);
	}
	
	public void registerCustomer(Customer customer) throws DBException
	{
		customerDao.persist(customer);
	}
	
	public void removeCustomer(Customer customer) throws DBException
	{
		customerDao.remove(customer);
	}
	
	public Customer updatePassword(Customer customer, Password newPassword) throws DBException
	{
		//I manage to create a new object. to avoid side effects issues.
		Customer new_state = new Customer(customer.getCustomerId(),customer.getName(),customer.getEmail(),newPassword,customer.isSubscrived());
		customerDao.persist(new_state);
		return new_state;
	}
	
	public Customer updateEmail(Customer customer, Email newEmail) throws DBException
	{
		//I manage to create a new object. to avoid side effects issues.
		Customer new_state = new Customer(customer.getCustomerId(),customer.getName(),newEmail,customer.getPassword(),customer.isSubscrived());
		customerDao.persist(new_state);
		return new_state;
	}
	
	public void unsubscriveFromMailingList(Customer customer) throws DBException, EmailNotSubscrivedException
	{
		emailService.unsubscriveFromEmailList(customer.getEmail());
	}
	
}
