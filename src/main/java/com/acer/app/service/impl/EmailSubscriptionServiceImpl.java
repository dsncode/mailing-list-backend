package com.acer.app.service.impl;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.acer.app.beans.Customer;
import com.acer.app.beans.value.Email;
import com.acer.app.dao.CustomerDao;
import com.acer.app.dao.EmailListDao;
import com.acer.app.exception.DBException;
import com.acer.app.exception.EmailNotSubscrivedException;
import com.acer.app.service.EmailSubscriptionService;

public class EmailSubscriptionServiceImpl implements EmailSubscriptionService{

	private EmailListDao emailListDao;
	
	public static EmailSubscriptionServiceImpl build(EmailListDao emailListDao)
	{
		return new EmailSubscriptionServiceImpl(emailListDao);
	}
	
	private EmailSubscriptionServiceImpl(EmailListDao emailListDao){
		this.emailListDao=emailListDao;
	}
	
	public Set<Email> getEmailSubscrivedList()
	{
		return emailListDao.getSubscribedList().stream().collect(Collectors.toSet());
	}
	
	public void sendEmailToAllSubscrivedList(String content)
	{
		getEmailSubscrivedList().stream().forEach(mail->sendEmail(mail,content));
	}
	
	public void sendEmail(Email email,String content)
	{
		//sends an email...
		System.out.println("email sent to "+email.getValue()+"...");
	}
	
	public boolean isEmailSubscribed(Email email) throws DBException
	{
		return emailListDao.isSubscrived(email);
	}
	
	public void unsubscriveFromEmailList(Email email) throws DBException, EmailNotSubscrivedException{
		
		emailListDao.unSubscriveFromMaillingList(email);
	}	
	
	public void subscriveToEmailList(Email email) throws DBException{
		
		
	}
	
		
}
