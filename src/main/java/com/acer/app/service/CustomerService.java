package com.acer.app.service;

import com.acer.app.beans.Customer;
import com.acer.app.beans.value.CustomerId;
import com.acer.app.beans.value.Email;
import com.acer.app.beans.value.Password;
import com.acer.app.dao.CustomerDao;
import com.acer.app.dao.impl.CustomerDaoImpl;
import com.acer.app.exception.CustomerException;
import com.acer.app.exception.DBException;
import com.acer.app.exception.EmailNotSubscrivedException;

public interface CustomerService {

	public Customer findCustomer(CustomerId id) throws CustomerException;
	
	public void registerCustomer(Customer customer) throws DBException;
	public void removeCustomer(Customer customer) throws DBException;
	public Customer updatePassword(Customer customer, Password newPassword) throws DBException;
	
	public Customer updateEmail(Customer customer, Email newEmail) throws DBException;
	
	public void unsubscriveFromMailingList(Customer customer) throws DBException, EmailNotSubscrivedException;
	
}
