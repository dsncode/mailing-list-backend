package com.acer.app.service;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.acer.app.beans.Customer;
import com.acer.app.beans.value.Email;
import com.acer.app.dao.CustomerDao;
import com.acer.app.exception.DBException;
import com.acer.app.exception.EmailNotSubscrivedException;

public interface EmailSubscriptionService {

	
	public Set<Email> getEmailSubscrivedList();
	
	public void sendEmailToAllSubscrivedList(String content);
	
	public void sendEmail(Email email,String content);
	
	public boolean isEmailSubscribed(Email email) throws DBException;
	public void unsubscriveFromEmailList(Email email) throws DBException, EmailNotSubscrivedException;
	public void subscriveToEmailList(Email email) throws DBException, EmailNotSubscrivedException;
		
}
