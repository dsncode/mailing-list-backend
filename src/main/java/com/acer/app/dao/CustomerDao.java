package com.acer.app.dao;

import java.util.Set;

import com.acer.app.beans.Customer;
import com.acer.app.beans.value.CustomerId;
import com.acer.app.exception.CustomerException;
import com.acer.app.exception.DBException;

public interface CustomerDao {

	public Customer findCustomer(CustomerId id) throws CustomerException;
	public void persist(Customer customer) throws DBException;
	public void remove(Customer customer) throws DBException;
	public Set<Customer> getCustomers();
	
}
