package com.acer.app.dao;

import java.util.Set;

import com.acer.app.beans.value.Email;
import com.acer.app.exception.DBException;
import com.acer.app.exception.EmailNotSubscrivedException;

public interface EmailListDao {

	public void subscriveToMailingList(Email email) throws DBException;
	public void unSubscriveFromMaillingList(Email email) throws EmailNotSubscrivedException, DBException;
	public Set<Email> getSubscribedList();
	public boolean isSubscrived(Email email) throws DBException;
	
}
