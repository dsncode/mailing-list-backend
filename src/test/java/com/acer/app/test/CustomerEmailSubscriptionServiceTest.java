package com.acer.app.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.acer.app.beans.value.Email;
import com.acer.app.dao.impl.EmailListDaoImpl;
import com.acer.app.exception.DBException;
import com.acer.app.exception.EmailNotSubscrivedException;
import com.acer.app.exception.InvalidEmailException;
import com.acer.app.service.EmailSubscriptionService;
import com.acer.app.service.impl.EmailSubscriptionServiceImpl;

public class CustomerEmailSubscriptionServiceTest {

	EmailSubscriptionService customerEmailService = EmailSubscriptionServiceImpl.build(EmailListDaoImpl.getInstance());
	
	@Test
	public void unsuscribe() throws InvalidEmailException, DBException, EmailNotSubscrivedException
	{
		Email email = Email.build("min@email.com");
		customerEmailService.unsubscriveFromEmailList(email);
		assertEquals(customerEmailService.isEmailSubscribed(email),false);
	}
	
	
	
}
