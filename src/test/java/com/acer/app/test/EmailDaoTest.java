package com.acer.app.test;

import org.junit.Test;

import com.acer.app.beans.value.Email;
import com.acer.app.dao.EmailListDao;
import com.acer.app.dao.impl.EmailListDaoImpl;
import com.acer.app.exception.DBException;
import com.acer.app.exception.EmailNotSubscrivedException;
import com.acer.app.exception.InvalidEmailException;

import static org.junit.Assert.assertEquals;

public class EmailDaoTest {

	EmailListDao emailDao = EmailListDaoImpl.getInstance();
	
	@Test
	public void emailSubscrived() throws InvalidEmailException, DBException
	{
		boolean subscrived = emailDao.isSubscrived(Email.build("d.silva@live.cl"));
		assertEquals(subscrived,true);
	}
	@Test
	public void subscriveMail() throws DBException, InvalidEmailException
	{
		Email email = Email.build("dummy@live.cl");
		emailDao.subscriveToMailingList(email);
		assertEquals(emailDao.isSubscrived(email),true);
	}
	
	@Test
	public void subscriveAndUnsubscrive() throws InvalidEmailException, DBException, EmailNotSubscrivedException
	{
		Email email = Email.build("dummy@live.cl");
		emailDao.subscriveToMailingList(email);
		assertEquals(emailDao.isSubscrived(email),true);
		emailDao.unSubscriveFromMaillingList(email);
		assertEquals(emailDao.isSubscrived(email),false);
	}
	
}
