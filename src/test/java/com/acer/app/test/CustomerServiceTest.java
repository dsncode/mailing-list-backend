package com.acer.app.test;

import org.junit.Test;

import com.acer.app.beans.Customer;
import com.acer.app.beans.value.CustomerId;
import com.acer.app.beans.value.Email;
import com.acer.app.beans.value.Password;
import com.acer.app.dao.CustomerDao;
import com.acer.app.dao.EmailListDao;
import com.acer.app.dao.impl.CustomerDaoImpl;
import com.acer.app.dao.impl.EmailListDaoImpl;
import com.acer.app.exception.CustomerException;
import com.acer.app.exception.CustomerNotFoundException;
import com.acer.app.exception.DBException;
import com.acer.app.service.CustomerService;
import com.acer.app.service.impl.CustomerServiceImpl;
import com.acer.app.service.impl.EmailSubscriptionServiceImpl;

public class CustomerServiceTest {

    CustomerDao customerDao = CustomerDaoImpl.getInstance();
    EmailListDao emailListDao = EmailListDaoImpl.getInstance();
	CustomerService customerService = CustomerServiceImpl.build(customerDao,EmailSubscriptionServiceImpl.build(emailListDao));
	
	
	@Test
	public void findUser() throws CustomerException
	{
		customerService.findCustomer(CustomerId.build("IDAAA"));
	}
	
	@Test(expected = CustomerNotFoundException.class) 
	public void removeUser() throws CustomerException, DBException
	{
		Customer jenny = customerService.findCustomer(CustomerId.build("IDCCC"));
		customerService.removeCustomer(jenny);
		customerService.findCustomer(CustomerId.build("IDCCC"));
	}
	
	@Test
	public void registerUser() throws DBException, CustomerException
	{
		Customer jack = new Customer(CustomerId.build("AAA"), "Jack", Email.build("jack@live.cl"), Password.build("123@abcABC"),true);
		customerService.registerCustomer(jack);
		customerService.findCustomer(CustomerId.build("AAA"));
	}
	
}
