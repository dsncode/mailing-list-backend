package com.acer.app.test;

import org.junit.Test;

import com.acer.app.beans.value.Email;
import com.acer.app.beans.value.Password;
import com.acer.app.exception.InvalidEmailException;
import com.acer.app.exception.InvalidPasswordException;

public class ValueObjectsTestSuite {

	@Test(expected = InvalidPasswordException.class)
	public void invalidPasswordValidation() throws InvalidPasswordException
	{
		Password.build("123");
	}
	
	@Test
	public void validPassword() throws InvalidPasswordException
	{
		Password.build("123@abcABC");
	}

	@Test(expected = InvalidEmailException.class)
	public void invalidEmail() throws InvalidEmailException
	{
		Email.build("d.silva@live");
	}
	
	
}
